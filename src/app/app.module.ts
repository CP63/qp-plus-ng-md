import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core"; // TODO: Need Input ?
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import {
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatSelectModule,
  MatTabsModule,
  MatTooltipModule,
  MAT_DATE_LOCALE
} from "@angular/material";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { FlexLayoutModule } from "@angular/flex-layout";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import "hammerjs";

/*==============================
=            ROUTES            =
==============================*/
import { RouterModule, Routes } from "@angular/router";
import { AppComponent } from "./app.component";
import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { NotFoundComponent } from "./not-found/not-found.component";
/*===== Registration =====*/
import { QuestionRegistrationComponent } from "./registration/question-registration.component";
import { DialogActionsEditQpAttComponent } from "./registration/dialogs/dialog-actions-edit-qp-att/dialog-actions-edit-qp-att.component";
import { DialogActionsEditWebformComponent } from "./registration/dialogs/dialog-actions-edit-webform/dialog-actions-edit-webform.component";
import { DialogActionsEditQoComponent } from "./registration/dialogs/dialog-actions-edit-qo/dialog-actions-edit-qo.component";
import { DialogActionsEditIkComponent } from "./registration/dialogs/dialog-actions-edit-ik/dialog-actions-edit-ik.component";
import { DialogActionsEditIgComponent } from "./registration/dialogs/dialog-actions-edit-ig/dialog-actions-edit-ig.component";
import { DialogActionsCreateRevisionQpAttComponent } from "./registration/dialogs/dialog-actions-create-revision-qp-att/dialog-actions-create-revision-qp-att.component";
import { DialogActionsCreateRevisionQpWebComponent } from "./registration/dialogs/dialog-actions-create-revision-qp-web/dialog-actions-create-revision-qp-web.component";
import { DialogActionsCreateRevisionQoComponent } from "./registration/dialogs/dialog-actions-create-revision-qo/dialog-actions-create-revision-qo.component";
import { DialogActionsCreateRevisionIkComponent } from "./registration/dialogs/dialog-actions-create-revision-ik/dialog-actions-create-revision-ik.component";
import { DialogActionsCreateRevisionIgComponent } from "./registration/dialogs/dialog-actions-create-revision-ig/dialog-actions-create-revision-ig.component";
import { DialogActionsCancelQuestionComponent } from "./registration/dialogs/dialog-actions-cancel-question/dialog-actions-cancel-question.component";

/*===== Translation =====*/

import { QuestionTranslationComponent } from "./translation/question-translation.component";
/*===== Admissibility =====*/

import { QuestionAdmissibilityAdministratorComponent } from "./admissibility/question-admissibility-administrator.component";
import { DialogActionsMaintainComponent } from "./admissibility/dialogs/dialog-actions-maintain/dialog-actions-maintain.component";

/*===== Precedent =====*/

import { PrecedentComponent } from "./precedent/precedent.component";
/*===== Diffusion =====*/

import { DiffusionComponent } from "./diffusion/diffusion.component";
/*===== Answer =====*/

import { AnswerComponent } from "./answer/answer.component";
/*===== History =====*/

import { HistoryComponent } from "./history/history.component";
import { LoaderComponent } from "./common/loader/loader.component";

const routes: Routes = [
  {
    path: "question-registration",
    component: QuestionRegistrationComponent
  },

  /*
  { path: "question-translation", component: QuestionTranslationComponent },
  */
  {
    path: "",
    redirectTo: "question-registration",
    pathMatch: "full"
  },
  {
    path: "**",
    component: NotFoundComponent
  } /* We can redirect to '' to not see the error message */
];

/*=====  End of ROUTES  ======*/

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NotFoundComponent,
    /*===== Registration =====*/
    QuestionRegistrationComponent,
    QuestionTranslationComponent,
    DialogActionsEditQpAttComponent,
    DialogActionsEditWebformComponent,
    DialogActionsEditQoComponent,
    DialogActionsEditIkComponent,
    DialogActionsEditIgComponent,
    DialogActionsCreateRevisionQpAttComponent,
    DialogActionsCreateRevisionQpWebComponent,
    DialogActionsCreateRevisionQoComponent,
    DialogActionsCreateRevisionIkComponent,
    DialogActionsCreateRevisionIgComponent,
    DialogActionsCancelQuestionComponent,
    /*===== END Registration =====*/
    /*===== Admissibility =====*/
    QuestionAdmissibilityAdministratorComponent,
    DialogActionsMaintainComponent,
    /*===== END Admissibility =====*/
    PrecedentComponent,
    DiffusionComponent,
    AnswerComponent,
    HistoryComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatTabsModule,
    MatTooltipModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    RouterModule.forRoot(routes)
  ],
  entryComponents: [
    /*===== Registration =====*/
    DialogActionsEditQpAttComponent,
    DialogActionsEditWebformComponent,
    DialogActionsEditQoComponent,
    DialogActionsEditIkComponent,
    DialogActionsEditIgComponent,
    DialogActionsCreateRevisionQpAttComponent,
    DialogActionsCreateRevisionQpWebComponent,
    DialogActionsCreateRevisionQoComponent,
    DialogActionsCreateRevisionIkComponent,
    DialogActionsCreateRevisionIgComponent,
    DialogActionsCancelQuestionComponent,
    /*===== END Registration =====*/
    /*===== Admissibility =====*/
    DialogActionsMaintainComponent
    /*===== END Admissibility =====*/
  ],
  providers: [
    {
      provide: MAT_DATE_LOCALE,
      useValue: "gb-GB" // For the MatDatePicker
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
