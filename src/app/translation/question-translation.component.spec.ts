import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionTranslationComponent } from './question-translation.component';

describe('QuestionTranslationComponent', () => {
  let component: QuestionTranslationComponent;
  let fixture: ComponentFixture<QuestionTranslationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionTranslationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionTranslationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
