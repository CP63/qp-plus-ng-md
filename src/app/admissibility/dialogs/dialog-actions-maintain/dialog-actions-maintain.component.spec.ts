import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogActionsMaintainComponent } from './dialog-actions-maintain.component';

describe('DialogActionsMaintainComponent', () => {
  let component: DialogActionsMaintainComponent;
  let fixture: ComponentFixture<DialogActionsMaintainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogActionsMaintainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogActionsMaintainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
