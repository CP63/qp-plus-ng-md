import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { MAT_DIALOG_DATA } from "@angular/material";
@Component({
	selector: "app-dialog-actions-maintain",
	templateUrl: "./dialog-actions-maintain.component.html",
	styleUrls: ["./dialog-actions-maintain.component.css"]
})
export class DialogActionsMaintainComponent implements OnInit {
	constructor(
		public thisDialogRef: MatDialogRef<
			DialogActionsMaintainComponent
		>,
		@Inject(MAT_DIALOG_DATA) public data: string
	) {}
	ngOnInit() {}

	onCloseMaintain() {
		this.thisDialogRef.close("Maintain");
	}

	/*==============================
	=            FIELDS            =
	==============================*/

	reasonsMaintaining = [
		{ value: "0", viewValue: "Select..." },
		{ value: "0", viewValue: "ADMISSIBILITY : ADMISSIBLE MACTU" },
		{ value: "0", viewValue: "ADMISSIBILITY : PC = ADMISSIBLE" },
		{ value: "0", viewValue: "ADMISSIBILITY : PC = ADMISSIBLE DIR/DG" },
		{ value: "0", viewValue: "DATA PROTECTION : ADMISSIBLE MACTU" },
		{ value: "0", viewValue: "DATA PROTECTION : PC = ADMISSIBLE" },
		{ value: "0", viewValue: "DATA PROTECTION : PC = ADMISSIBLE DIR/DG" },
		{ value: "0", viewValue: "OFFENSIF LANGUAGE : ADMISSIBLE MACTU" },
		{ value: "0", viewValue: "OFFENSIF LANGUAGE : PC = ADMISSIBLE" },
		{ value: "0", viewValue: "OFFENSIF LANGUAGE : PC = ADMISSIBLE DIR/DG" },
		{ value: "0", viewValue: "PEC : ADMISSIBLE MACTU" },
		{ value: "0", viewValue: "PEC : PC = ADMISSIBLE" },
		{ value: "0", viewValue: "PEC : PC = ADMISSIBLE DIR/DG" },
		{ value: "0", viewValue: "SIMILAR : ADMISSIBLE MACTU" },
		{ value: "0", viewValue: "SIMILAR : CP = ADMISSIBLE" },
		{ value: "0", viewValue: "SIMILAR : CP = ADMISSIBLE DIR/DG" },
		{ value: "0", viewValue: "FACTUAL INFO : ADMISSIBLE MACTU" },
		{ value: "0", viewValue: "FACTUAL INFO : PC = ADMISSIBLE" },
		{ value: "0", viewValue: "FACTUAL INFO : PC = ADMISSIBLE DIR/DG" },
		{ value: "0", viewValue: "RELATED SUBJECTS : ADMISSIBLE" },
		{ value: "0", viewValue: "RELATED SUBJECTS : PC = ADMISSIBLE" },
		{ value: "0", viewValue: "RELATED SUBJECTS : PC = ADMISSIBLE DIR/DG" },
		{ value: "0", viewValue: "OTHER" }
	];
	/*=====  End of FIELDS  ======*/
}
