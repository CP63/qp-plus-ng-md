import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionAdmissibilityAdministratorComponent } from './question-admissibility-administrator.component';

describe('QuestionAdmissibilityAdministratorComponent', () => {
  let component: QuestionAdmissibilityAdministratorComponent;
  let fixture: ComponentFixture<QuestionAdmissibilityAdministratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionAdmissibilityAdministratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionAdmissibilityAdministratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
