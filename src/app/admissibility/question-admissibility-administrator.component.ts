import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import { DialogActionsMaintainComponent } from "@app/admissibility/dialogs/dialog-actions-maintain/dialog-actions-maintain.component";

@Component({
	selector: "app-question-admissibility-administrator",
	templateUrl: "./question-admissibility-administrator.component.html",
	styleUrls: ["./question-admissibility-administrator.component.css"]
})
export class QuestionAdmissibilityAdministratorComponent implements OnInit {
	dialogResult = "";

	constructor(public dialog: MatDialog) {}

	ngOnInit() {}

  openDialogMaintain() {
    let dialogRef = this.dialog.open(DialogActionsMaintainComponent, {
      width: "60%"
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog closed: ${result}`);
      this.dialogResult = result;
    });
  }

	/*==================================
  =            DATEPICKER            =
  ==================================*/
	filterWithoutSatSun = (d: Date): boolean => {
		const day = d.getDay();
		// Prevent Saturday and Sunday from being selected.
		return day !== 0 && day !== 6;
	};
	/*===================================================
  =            ACTIONS PANEL BUTTONS CLICK            =
  ===================================================*/

	/*======= Suspended status =======*/

	isSuspended = false;

	showIsSuspended() {
		this.isSuspended = !this.isSuspended;
		this.isRevisionAccepted = !this.isSuspended;
	}

	/*======= Revision Accepted status =======*/

	isRevisionAccepted = false;

	showIsRevisionAccepted() {
		this.isRevisionAccepted = !this.isRevisionAccepted;
		this.isSuspended = !this.isRevisionAccepted;
	}
}
