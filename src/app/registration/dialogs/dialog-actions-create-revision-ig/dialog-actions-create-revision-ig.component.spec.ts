import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogActionsCreateRevisionIgComponent } from './dialog-actions-create-revision-ig.component';

describe('DialogActionsCreateRevisionIgComponent', () => {
  let component: DialogActionsCreateRevisionIgComponent;
  let fixture: ComponentFixture<DialogActionsCreateRevisionIgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogActionsCreateRevisionIgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogActionsCreateRevisionIgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
