import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { MAT_DIALOG_DATA } from "@angular/material";
@Component({
	selector: "app-dialog-actions-cancel-question",
	templateUrl: "./dialog-actions-cancel-question.component.html",
	styleUrls: ["./dialog-actions-cancel-question.component.css"]
})
export class DialogActionsCancelQuestionComponent implements OnInit {
	constructor(
		public thisDialogRef: MatDialogRef<
			DialogActionsCancelQuestionComponent
		>,
		@Inject(MAT_DIALOG_DATA) public data: string
	) {}
	ngOnInit() {}

	onCloseCancelQuestion() {
		this.thisDialogRef.close("CancelQuestion");
	}

	/*==============================
	=            FIELDS            =
	==============================*/
	reasonsCancelling = [
		{ value: "0", viewValue: "Select..." },
		{ value: "0", viewValue: "AUTHOR : WITHDRAWN" },
		{ value: "0", viewValue: "REGISTERED TWICE" },
		{ value: "0", viewValue: "ADMISSIBILITY : WITHDRAWN" },
		{ value: "0", viewValue: "ADMISSIBILITY : PC = INADMISSIBLE" },
		{ value: "0", viewValue: "ADMISSIBILITY : PC = WITHDRAWN" },
		{ value: "0", viewValue: "DATA PROTECTION : WITHDRAWN" },
		{ value: "0", viewValue: "DATE PROTECTION : PC = INADMISSIBLE" },
		{ value: "0", viewValue: "DATA PROTECTION : PC = WITHDRAWN" },
		{ value: "0", viewValue: "OFFENSIF LANGUAGE : WITHDRAWN" },
		{ value: "0", viewValue: "OFFENSIF LANGUAGE : PC = INADMISSIBLE" },
		{ value: "0", viewValue: "OFFENSIF LANGUAGE : PC = WITHDRAWN" },
		{ value: "0", viewValue: "PEC : WITHDRAWN" },
		{ value: "0", viewValue: "PEC : PC = INADMISSIBLE" },
		{ value: "0", viewValue: "PEC : PC = WITHDRAWN" },
		{ value: "0", viewValue: "SIMILAR : WITHDRAWN" },
		{ value: "0", viewValue: "SIMILAR : WITHDRAWN NO ANSWER" },
		{ value: "0", viewValue: "SIMILAR : PC = INADMISSIBLE" },
		{ value: "0", viewValue: "SIMILAR : PC = WITHDRAWN" },
		{ value: "0", viewValue: "FACTUAL INFO : WITHDRAWN" },
		{ value: "0", viewValue: "FACTUAL INFO : WITHDRAWN NO ANSWER" },
		{ value: "0", viewValue: "FACTUAL INFO : PC = INADMISSIBLE" },
		{ value: "0", viewValue: "FACTUAL INFO : PC = WITHDRAWN" },
		{ value: "0", viewValue: "RELATED SUBJECTS : WITHDRAWN" },
		{ value: "0", viewValue: "RELATED SUBJECTS : RETABLED" },
		{ value: "0", viewValue: "RELATED SUBJECTS : PC = INADMISSIBLE" },
		{ value: "0", viewValue: "RELATED SUBJECTS : PC = WITHDRAWN" },
		{ value: "0", viewValue: "RELATED SUBJECTS : PC= RETABL" }
	];
	/*=====  End of FIELDS  ======*/
}
