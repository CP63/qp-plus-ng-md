import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogActionsCancelQuestionComponent } from './dialog-actions-cancel-question.component';

describe('DialogActionsCancelQuestionComponent', () => {
  let component: DialogActionsCancelQuestionComponent;
  let fixture: ComponentFixture<DialogActionsCancelQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogActionsCancelQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogActionsCancelQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
