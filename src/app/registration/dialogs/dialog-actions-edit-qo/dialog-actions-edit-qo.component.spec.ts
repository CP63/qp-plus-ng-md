import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogActionsEditQoComponent } from './dialog-actions-edit-qo.component';

describe('DialogActionsEditQoComponent', () => {
  let component: DialogActionsEditQoComponent;
  let fixture: ComponentFixture<DialogActionsEditQoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogActionsEditQoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogActionsEditQoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
