import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogActionsCreateRevisionQpAttComponent } from './dialog-actions-create-revision-qp-att.component';

describe('DialogActionsCreateRevisionQpAttComponent', () => {
  let component: DialogActionsCreateRevisionQpAttComponent;
  let fixture: ComponentFixture<DialogActionsCreateRevisionQpAttComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogActionsCreateRevisionQpAttComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogActionsCreateRevisionQpAttComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
