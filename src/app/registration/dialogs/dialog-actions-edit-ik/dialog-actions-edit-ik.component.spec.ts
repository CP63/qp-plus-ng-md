import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogActionsEditIkComponent } from './dialog-actions-edit-ik.component';

describe('DialogActionsEditIkComponent', () => {
  let component: DialogActionsEditIkComponent;
  let fixture: ComponentFixture<DialogActionsEditIkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogActionsEditIkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogActionsEditIkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
