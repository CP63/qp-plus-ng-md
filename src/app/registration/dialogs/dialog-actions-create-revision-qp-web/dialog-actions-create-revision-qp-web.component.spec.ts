import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogActionsCreateRevisionQpWebComponent } from './dialog-actions-create-revision-qp-web.component';

describe('DialogActionsCreateRevisionQpWebComponent', () => {
  let component: DialogActionsCreateRevisionQpWebComponent;
  let fixture: ComponentFixture<DialogActionsCreateRevisionQpWebComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogActionsCreateRevisionQpWebComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogActionsCreateRevisionQpWebComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
