import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogActionsEditIgComponent } from './dialog-actions-edit-ig.component';

describe('DialogActionsEditIgComponent', () => {
  let component: DialogActionsEditIgComponent;
  let fixture: ComponentFixture<DialogActionsEditIgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogActionsEditIgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogActionsEditIgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
