import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogActionsCreateRevisionIkComponent } from './dialog-actions-create-revision-ik.component';

describe('DialogActionsCreateRevisionIkComponent', () => {
  let component: DialogActionsCreateRevisionIkComponent;
  let fixture: ComponentFixture<DialogActionsCreateRevisionIkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogActionsCreateRevisionIkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogActionsCreateRevisionIkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
