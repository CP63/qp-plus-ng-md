import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogActionsEditQpAttComponent } from './dialog-actions-edit-qp-att.component';

describe('DialogActionsEditQpAttComponent', () => {
  let component: DialogActionsEditQpAttComponent;
  let fixture: ComponentFixture<DialogActionsEditQpAttComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogActionsEditQpAttComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogActionsEditQpAttComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
