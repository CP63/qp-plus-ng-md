import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogActionsCreateRevisionQoComponent } from './dialog-actions-create-revision-qo.component';

describe('DialogActionsCreateRevisionQoComponent', () => {
  let component: DialogActionsCreateRevisionQoComponent;
  let fixture: ComponentFixture<DialogActionsCreateRevisionQoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogActionsCreateRevisionQoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogActionsCreateRevisionQoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
