import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { MAT_DIALOG_DATA } from "@angular/material";

@Component({
  selector: "app-dialog-actions-create-revision-qo",
  templateUrl: "./dialog-actions-create-revision-qo.component.html",
  styleUrls: ["./dialog-actions-create-revision-qo.component.css"]
})
export class DialogActionsCreateRevisionQoComponent implements OnInit {
  constructor(
    public thisDialogRef: MatDialogRef<DialogActionsCreateRevisionQoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string
  ) {}

  ngOnInit() {}

  onCloseSaveNewRevision() {
    this.thisDialogRef.close("SaveNewRevision");
  }

  onCloseSave() {
    this.thisDialogRef.close("Save");
  }

  /*==================================
  =            DATEPICKER            =
  ==================================*/
  filterWithoutSatSun = (d: Date): boolean => {
    const day = d.getDay();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0 && day !== 6;
  };

  /*====================================
  =            SELECT FIELD            =
  ====================================*/
  types = [
    { value: "0", viewValue: "Written question" },
    { value: "1", viewValue: "Written question (Priority)" },
    { value: "2", viewValue: "..." }
  ];

  addressees = [
    { value: "0", viewValue: "President of the Council" },
    { value: "1", viewValue: "Council" },
    { value: "2", viewValue: "Commission" },
    {
      value: "3",
      viewValue:
        "The Vice-President of the Commission / High Representative of the Union for Foreign Affairs and Security Policy"
    }
  ];

  languages = [
    { value: "0", viewValue: "French" },
    { value: "1", viewValue: "English" },
    { value: "2", viewValue: "Greek" },
    { value: "3", viewValue: "Irish" },
    { value: "4", viewValue: "Portuguese" },
    { value: "5", viewValue: "Spanish" }
  ];

  hours = [
    // TODO: Delete this. Replace Select time with new module of Angular Material Datepicker with integrated time selection
    { value: "06", viewValue: "06" },
    { value: "07", viewValue: "07" },
    { value: "08", viewValue: "08" },
    { value: "09", viewValue: "09" },
    { value: "10", viewValue: "10" },
    { value: "11", viewValue: "11" },
    { value: "12", viewValue: "12" },
    { value: "13", viewValue: "13" },
    { value: "14", viewValue: "14" },
    { value: "15", viewValue: "15" },
    { value: "16", viewValue: "16" },
    { value: "17", viewValue: "17" },
    { value: "18", viewValue: "18" },
    { value: "19", viewValue: "19" },
    { value: "20", viewValue: "20" },
    { value: "21", viewValue: "21" },
    { value: "22", viewValue: "22" }
  ];

  minutes = [
    // TODO: Delete this. Replace Select time with new module of Angular Material Datepicker with integrated time selection
    { value: "00", viewValue: "00" },
    { value: "05", viewValue: "05" },
    { value: "10", viewValue: "10" },
    { value: "15", viewValue: "15" },
    { value: "20", viewValue: "20" },
    { value: "25", viewValue: "25" },
    { value: "30", viewValue: "30" },
    { value: "35", viewValue: "35" },
    { value: "40", viewValue: "40" },
    { value: "45", viewValue: "45" },
    { value: "50", viewValue: "50" },
    { value: "55", viewValue: "55" }
  ];

  reasonsRevision = [
    { value: "0", viewValue: "ADMISSIBILITY : REVISED" },
    { value: "1", viewValue: "ADMISSIBILITY : PC = REVISED" },
    { value: "2", viewValue: "DATA PROTECTION : REVISED" },
    { value: "3", viewValue: "DATA PROTECTION : PC = REVISED" },
    { value: "4", viewValue: "OFFENSIF LANGUAGE : REVISED" },
    { value: "5", viewValue: "OFFENSIF LANGUAGE : PC = REVISED" },
    { value: "6", viewValue: "PEC : REVISED" },
    { value: "7", viewValue: "PEC : PC = REVISED" },
    { value: "8", viewValue: "SIMILAR : REVISED" },
    { value: "9", viewValue: "SIMILAR : PC = REVISED" },
    { value: "10", viewValue: "FACTUAL INFO : REVISED" },
    { value: "11", viewValue: "FACTUAL INFO : PC = REVISED" },
    { value: "12", viewValue: "RELATED SUBJECTS : REVISED" },
    { value: "13", viewValue: "RELATED SUBJECTS : PC = REVISED" },
    { value: "14", viewValue: "OTHER" }
  ];
  /*=====  End of SELECT FIELD  ======*/
}
