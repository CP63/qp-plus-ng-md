import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogActionsEditWebformComponent } from './dialog-actions-edit-webform.component';

describe('DialogActionsEditWebformComponent', () => {
  let component: DialogActionsEditWebformComponent;
  let fixture: ComponentFixture<DialogActionsEditWebformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogActionsEditWebformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogActionsEditWebformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
