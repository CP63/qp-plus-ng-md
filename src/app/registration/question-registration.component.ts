import { Component, OnInit, Input } from "@angular/core";
import { MatDialog } from "@angular/material";
import { DialogActionsEditQpAttComponent } from "@app/registration/dialogs/dialog-actions-edit-qp-att/dialog-actions-edit-qp-att.component";
import { DialogActionsEditWebformComponent } from "@app/registration/dialogs/dialog-actions-edit-webform/dialog-actions-edit-webform.component";
import { DialogActionsEditQoComponent } from "@app/registration/dialogs/dialog-actions-edit-qo/dialog-actions-edit-qo.component";
import { DialogActionsEditIkComponent } from "@app/registration/dialogs/dialog-actions-edit-ik/dialog-actions-edit-ik.component";
import { DialogActionsEditIgComponent } from "@app/registration/dialogs/dialog-actions-edit-ig/dialog-actions-edit-ig.component";
import { DialogActionsCreateRevisionQpAttComponent } from "@app/registration/dialogs/dialog-actions-create-revision-qp-att/dialog-actions-create-revision-qp-att.component";
import { DialogActionsCreateRevisionQpWebComponent } from "@app/registration/dialogs/dialog-actions-create-revision-qp-web/dialog-actions-create-revision-qp-web.component";
import { DialogActionsCreateRevisionQoComponent } from "@app/registration/dialogs/dialog-actions-create-revision-qo/dialog-actions-create-revision-qo.component";
import { DialogActionsCreateRevisionIkComponent } from "@app/registration/dialogs/dialog-actions-create-revision-ik/dialog-actions-create-revision-ik.component";
import { DialogActionsCreateRevisionIgComponent } from "@app/registration/dialogs/dialog-actions-create-revision-ig/dialog-actions-create-revision-ig.component";
import { DialogActionsCancelQuestionComponent } from "@app/registration/dialogs/dialog-actions-cancel-question/dialog-actions-cancel-question.component";

@Component({
  selector: "app-question-registration",
  templateUrl: "./question-registration.component.html",
  styleUrls: ["./question-registration.component.css"]
})
export class QuestionRegistrationComponent implements OnInit {
  dialogResult = "";

  @Input() public pageTypeQuestionValue: String;

  constructor(public dialog: MatDialog) {}

  ngOnInit() {
    // temp
    /*
    this.dialog.open(DialogActionsEditQpAttComponent, {
      width: '60%',
    });
    */
  }

  openDialogEditQpAtt() {
    let dialogRef = this.dialog.open(DialogActionsEditQpAttComponent, {
      width: "60%"
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog closed: ${result}`);
      this.dialogResult = result;
    });
  }

  openDialogEditWebform() {
    let dialogRef = this.dialog.open(DialogActionsEditWebformComponent, {
      width: "60%"
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog closed: ${result}`);
      this.dialogResult = result;
    });
  }

  openDialogEditQo() {
    let dialogRef = this.dialog.open(DialogActionsEditQoComponent, {
      width: "60%"
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog closed: ${result}`);
      this.dialogResult = result;
    });
  }

  openDialogEditIk() {
    let dialogRef = this.dialog.open(DialogActionsEditIkComponent, {
      width: "60%"
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog closed: ${result}`);
      this.dialogResult = result;
    });
  }

  openDialogEditIg() {
    let dialogRef = this.dialog.open(DialogActionsEditIgComponent, {
      width: "60%"
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog closed: ${result}`);
      this.dialogResult = result;
    });
  }

  openDialogCreateRevisionQpAtt() {
    let dialogRef = this.dialog.open(
      DialogActionsCreateRevisionQpAttComponent,
      {
        width: "60%"
      }
    );
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog closed: ${result}`);
      this.dialogResult = result;
    });
  }

  openDialogCreateRevisionQpWeb() {
    let dialogRef = this.dialog.open(
      DialogActionsCreateRevisionQpWebComponent,
      {
        width: "60%"
      }
    );
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog closed: ${result}`);
      this.dialogResult = result;
    });
  }

  openDialogCreateRevisionQo() {
    let dialogRef = this.dialog.open(DialogActionsCreateRevisionQoComponent, {
      width: "60%"
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog closed: ${result}`);
      this.dialogResult = result;
    });
  }

  openDialogCreateRevisionIk() {
    let dialogRef = this.dialog.open(DialogActionsCreateRevisionIkComponent, {
      width: "60%"
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog closed: ${result}`);
      this.dialogResult = result;
    });
  }

  openDialogCreateRevisionIg() {
    let dialogRef = this.dialog.open(DialogActionsCreateRevisionIgComponent, {
      width: "60%"
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog closed: ${result}`);
      this.dialogResult = result;
    });
  }

  openDialogCancelQuestion() {
    let dialogRef = this.dialog.open(DialogActionsCancelQuestionComponent, {
      width: "60%"
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog closed: ${result}`);
      this.dialogResult = result;
    });
  }

  /*===================================================
  =            ACTIONS PANEL BUTTONS CLICK            =
  ===================================================*/

  /*======= Registration status =======*/

  isRegistered = false;

  showIsRegistered() {
    this.isRegistered = !this.isRegistered;
  }

  /*======= Send Back to MEP status =======*/

  isSentBackToMep = false;

  showIsSentBackToMep() {
    this.isSentBackToMep = !this.isSentBackToMep;
  }

  /*======= Reserve for Registration =======*/

  isReservedRegistration = false;

  showIsReservedRegistration() {
    this.isReservedRegistration = !this.isReservedRegistration;
  }

  /*=======================================================
  =            END ACTIONS PANEL BUTTONS CLICK            =
  =======================================================*/

  pageTypeQuestion: any[] = [
    {
      shortLabel: "qpAtt",
      longLabel: "QP Attachement",
      iconLabel: "P",
      colorLabel: "#c62828"
    },
    {
      shortLabel: "qpWeb",
      longLabel: "QP Webform",
      iconLabel: "P",
      colorLabel: "#c62828"
    },
    {
      shortLabel: "qeAtt",
      longLabel: "QE Attachement",
      iconLabel: "E",
      colorLabel: "#00bfd3"
    },
    {
      shortLabel: "qeWeb",
      longLabel: "QE Webform",
      iconLabel: "E",
      colorLabel: "#00bfd3"
    },
    {
      shortLabel: "qo",
      longLabel: "QO",
      iconLabel: "O",
      colorLabel: "#689f38"
    },
    {
      shortLabel: "ik",
      longLabel: "IK",
      iconLabel: "K",
      colorLabel: "#9e9d24"
    },
    {
      shortLabel: "ig",
      longLabel: "IG",
      iconLabel: "G",
      colorLabel: "#c2185b"
    }
  ];
}
