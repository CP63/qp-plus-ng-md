FROM node:8
WORKDIR /qpplus

COPY package.json /qpplus
RUN npm install
COPY . /qpplus

CMD ["npm", "start"]

EXPOSE 4222
